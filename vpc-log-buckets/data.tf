data "aws_iam_policy_document" "vpc_log" {
  statement {
    sid    = "AWSLogDeliveryAclCheck"
    effect = "Allow"
    principals {
      identifiers = ["delivery.logs.amazonaws.com"]
      type        = "service"
    }
    actions   = ["s3:GetBucketAcl"]
    resources = ["arn:aws:s3:::TUW2-SECLOG-S3-VPCLOG"]
  }
  statement {
    sid    = "AWSLogDeliveryWrite20150319"
    effect = "Allow"
    principals {
      identifiers = ["delivery.logs.amazonaws.com"]
      type        = "service"
    }
    actions = ["s3:PutObject"]
    resources = [
      "arn:aws:s3:::TUW2-SECLOG-S3-VPCLOG/*",
    "arn:aws:s3:::TUW2-SECLOG-S3-VPCLOG"]
    condition {
      test     = "StringEquals"
      variable = "aws:PrincipalOrgID"
      values   = ["o-x8grae02fl"]
    }
  }
  statement {
    sid    = "AWSLogWrite"
    effect = "Allow"
    principals {
      identifiers = ["delivery.logs.amazonaws.com"]
      type        = "service"
    }
    actions = ["s3:PutObject"]
    resources = [
      "arn:aws:s3:::TUW2-SECLOG-S3-VPCLOG/*",
    "arn:aws:s3:::TUW2-SECLOG-S3-VPCLOG"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }

  }
}


