data "aws_iam_policy_document" "cloudtrail" {

  statement {
    sid    = "AWSCloudTrailAclCheck"
    effect = "Allow"
    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "service"
    }
    actions   = ["s3:GetBucketAcl"]
    resources = ["arn:aws:s3:::TUW2-SECLOG-S3-TRAIL"]
  }
  statement {
    sid    = "AWSCloudTrailWrite20150319"
    effect = "Allow"
    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "service"
    }
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::TUW2-SECLOG-S3-TRAIL/AWSLogs/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }

  statement {
    sid    = "AWSCloudTrailWrite"
    effect = "Allow"
    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "service"
    }
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::TUW2-SECLOG-S3-TRAIL/AWSLogs/o-x8grae02fl/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
}


/*data "aws_iam_policy_document" "cloudtrail_kms_key_policy" {
  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    principals {
      identifiers = ["cloudtrail.amazonaws.com"]
      type        = "service"
    }
    principals {
      identifiers = ["arn:aws:iam::295390451377:root",
      "arn:aws:iam::140199734014:root"]
      type = "aws"
    }
    actions   = ["kms:*"]
    resources = ["*"]
  }
}
*/


