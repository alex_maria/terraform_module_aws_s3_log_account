resource "aws_s3_bucket" "cloudtrail" {
  bucket = var.cloudtrail_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
/*  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.cloudtrail.id
        sse_algorithm     = "aws:kms"
      }
    }
  }*/
  lifecycle_rule {
    enabled = true
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}

resource "aws_s3_bucket_policy" "cloudtrail" {
  bucket = aws_s3_bucket.cloudtrail.id
  policy = data.aws_iam_policy_document.cloudtrail.json
}
