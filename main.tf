module "cloudtrail-bucket" {
  source = "./cloudtrail_bucket"

  region = "us-west-2"

  cloudtrail_bucket_name = "aws-p-usw2-rcpfeatlz-s3-traillogs"

  #provider_account_id = ""

  #aws_provider_assume_role_name = ""

}

module "vpc-log-bucket" {
  source = "./vpc-log-buckets"

  region = "us-west-2"

  vpc_log_bucket_name = "aws-p-usw2-rcpfeatlz-s3-vpcflowlogs"

  #provider_account_id = ""

  #aws_provider_assume_role_name = ""

}

module "config-bucket" {
  source = "./config_bucket"

  region = "us-west-2"

  config_bucket_name = "aws-p-usw2-rcpfeatlz-s3-configlogs"

  #provider_account_id = ""

  #aws_provider_assume_role_name = ""

}

module "security-hub-bucket" {
  source = "./security_hub_bucket"

  region = "us-west-2"

  security_hub_bucket_name = "aws-p-usw2-rcpfeatlz-s3-vpcflowlogs"

  #provider_account_id = ""

  #aws_provider_assume_role_name = ""

}