data "aws_iam_policy_document" "config" {
  statement {
    sid    = "AWSCloudTrailAclCheck"
    effect = "Allow"
    principals {
      identifiers = ["config.amazonaws.com"]
      type        = "service"
    }
    actions   = ["s3:GetBucketAcl"]
    resources = ["arn:aws:s3:::TUW2-SECLOG-S3-CONFIG"]
  }

  statement {
    sid    = "AWSCloudTrailWrite20150319"
    effect = "Allow"
    principals {
      identifiers = ["config.amazonaws.com"]
      type        = "service"
    }
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::TUW2-SECLOG-S3-CONFIG/AWSLogs/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }

  statement {
    sid    = "AWSCloudTrailWrite"
    effect = "Allow"
    principals {
      identifiers = ["config.amazonaws.com"]
      type        = "service"
    }
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::TUW2-SECLOG-S3-CONFIG/AWSLogs/o-x8grae02fl/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
}

