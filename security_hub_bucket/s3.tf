resource "aws_s3_bucket" "security_hub" {
  bucket = var.security_hub_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  lifecycle_rule {
    enabled = true
    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}

#resource "aws_s3_bucket_policy" "security_hub" {
# bucket = aws_s3_bucket.security_hub.id
#policy = data.aws_iam_policy_document.security_hub.json
#}

